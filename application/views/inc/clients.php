<!-- Clients -->
	<div class=" col-md-6 clients">
			<h3>Testimonials</h3>
			<section class="slider">
				<div class="flexslider">
					<ul class="slides">
						<li>
								<div class="client">
									<img src="<?php echo base_url('assets/images/t3.jpg');?>" alt="" />
									<h5>Metin ERBEK</h5>
									<div class="clearfix"> </div>
								</div>
								<p>Gidenle ile Endonezya'ya çok ucuz ve hızlı paket gönderebildiğim için çok mutluyum</p>
								
						</li>
						<li>	
								<div class="client">
								<img src="<?php echo base_url('assets/images/t2.jpg');?>" alt="" />
									<h5>Murat TURHAN</h5>
									<div class="clearfix"> </div>
								</div>
								<p>İzmir'e giderken boş gitmiyorum, benzin paramı götürdüğüm paketlerden çıkarıyorum..</p>
								
						</li>

					</ul>
				</div>
			</section>
			<br><br><br>
</div>
<!-- //Clients -->