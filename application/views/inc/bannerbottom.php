<!-- bootstrap-modal-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Transporters
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
					<div class="modal-body">
						<img src="images/bg3.jpg" alt=" " class="img-responsive" />
						<p>Ut enim ad minima veniam, quis nostrum 
							exercitationem ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi consequatur? Quis autem 
							vel eum iure reprehenderit qui in ea voluptate velit 
							esse quam nihil molestiae consequatur, vel illum qui 
							dolorem eum fugiat quo voluptas nulla pariatur.
							<i>" Quis autem vel eum iure reprehenderit qui in ea voluptate velit 
								esse quam nihil molestiae consequatur.</i></p>
					</div>
			</div>
		</div>
	</div>
<!-- //bootstrap-modal-pop-up --> 
<!-- banner-bottom -->
<div class="banner-bottom">
	<div class="col-md-7 bannerbottomleft">
			<div class="video-grid-single-page-agileits">
				<div data-video="d3q5mRA5djY" id="video"> <img src="images/bg2.jpg" alt="" class="img-responsive" /> </div>
			</div>
	</div>
	<div class="col-md-5 bannerbottomright">
		<h3>How Does We Work?</h3>
		<p>Ut enim ad minima veniam, quis nostrum 
			exercitationem ulla corporis suscipit laboriosam, 
			nisi ut aliquid ex ea.</p>
		<h4><i class="fa fa-taxi" aria-hidden="true"></i>International Transport Deliver System</h4>
		<h4><i class="fa fa-shield" aria-hidden="true"></i>Fast & Best Deliver Service</h4>
		<h4><i class="fa fa-ticket" aria-hidden="true"></i>Standard Courier value</h4>
		<h4><i class="fa fa-space-shuttle" aria-hidden="true"></i>Easy And Auto Shipping Service</h4>
		<h4><i class="fa fa-truck" aria-hidden="true"></i>Packaging & Storage</h4>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //banner-bottom -->