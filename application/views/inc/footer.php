<!-- footer -->
	<footer>
		<div class="agileits-w3layouts-footer">
			<div class="container">
				<div class="col-md-4 w3-agile-grid">
					<h5>Hakkımızda</h5>
					<p>Paket/kargo taşımada birbirine yardım etmeyi amaçlayan insanlarım platformu</p>
					<div class="footer-agileinfo-social">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 w3-agile-grid"></div>
				<div class="col-md-4 w3-agile-grid">
					<h5>Adres</h5>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Telefon</h6>
								<p>+90 543 302 96 62</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email</h6>
								<p>Email :<a href="mailto:info@gidenle.com"> info@gidenle.com</a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!--
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<p> SE10 8JQ, Greenwich Road, London. 
								Telephone : +0(12) 444 262 399
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						--->
					</div>
				</div>
				<!--
				<div class="col-md-4 w3-agile-grid">
				
					<h5>Recent Posts</h5>
					<div class="w3ls-post-grids">
						<div class="w3ls-post-grid">
							<div class="w3ls-post-img">
								<a href="#"><img src="images/p1.jpg" alt="" /></a>
							</div>
							<div class="w3ls-post-info">
								<h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
								<p>June 10,2017</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3ls-post-grid">
							<div class="w3ls-post-img">
								<a href="#"><img src="images/p2.jpg" alt="" /></a>
							</div>
							<div class="w3ls-post-info">
								<h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
								<p>June 17,2017</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3ls-post-grid">
							<div class="w3ls-post-img">
								<a href="#"><img src="images/p3.jpg" alt="" /></a>
							</div>
							<div class="w3ls-post-info">
								<h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
								<p>June 26,2017</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3ls-post-grid">
							<div class="w3ls-post-img">
								<a href="#"><img src="images/p1.jpg" alt="" /></a>
							</div>
							<div class="w3ls-post-info">
								<h6><a href="#" data-toggle="modal" data-target="#myModal">Donec vel sapien in erat</a></h6>
								<p>June 26,2017</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					
				</div>
				---->
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="copyright">
			<div class="container">
				<p>© 2018 Gidenle. All rights reserved | Bu Bir <span style="color:red">Codewarriors</span> İşidir.</p>
			</div>
		</div>
	</footer>
	<!-- //footer -->